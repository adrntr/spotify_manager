import spotipy
from spotipy.oauth2 import SpotifyOAuth

from Modules.playlist import Playlist, POPULARITY

if __name__ == "__main__":
    # Define the required scope for Spotify API access
    scope = "user-library-read, playlist-modify-public"

    # Create a Spotify instance with authentication
    sp = spotipy.Spotify(auth_manager=SpotifyOAuth(scope=scope,
                                                   client_id='See Readme',  # Replace with actual client ID
                                                   client_secret='See Readme',  # Replace with actual client secret
                                                   redirect_uri='http://localhost'))

    # Create a Playlist instance with the provided playlist ID and Spotify instance
    playlist = Playlist(playlist_id='See Readme', spotify=sp)  # Replace with actual playlist ID

    # Fetch tracks for the playlist from Spotify
    playlist.get_tracks_from_spotify()

    # Fetch additional track features from Spotify
    playlist.get_track_features()

    # Order tracks within the playlist based on popularity
    playlist.order_tracks(True, POPULARITY)  # Order in descending order based on popularity

    # Print the playlist details
    print(playlist)

    # Reorder the tracks on Spotify based on popularity in descending order
    playlist.order_tracks_on_spotify(reverse=True, sort_by=POPULARITY)
