 # Spotify Playlist Sorter
 
This repository contains a script to order your playlist in spotify base on the popularity, danceability, etc. of the 
songs

## Requirements
- Spotify account
- A playlist created

## How to

### Obtain your Client ID and Client Secret
1. Go to https://developer.spotify.com/
2. Login
3. Go to your profile -> Dashboard
4. Create App
   1. Set a name
   2. Set a Description
   3. Redirect URI: http://localhost
5. Go the Dashboard --> Your app --> Settings
6. Copy you Client ID and Client Secret (View Client Secret)

### Obtain the playlist ID
1. Go to the playlist in spotify
2. Right clic --> Share --> Copy Link
3. You will obtain something like this: https://open.spotify.com/playlist/6DE0o9Cza8nWP6slQSg2dM?si=a79de71dc4f9442d
4. The Playlist ID is the code between "playlist/" and "?", in this case: *6DE0o9Cza8nWP6slQSg2dM*

### Code example

Replace **your_client_id**, **your_client_secret** and **your_playlist_id** with the data obtained in the previous steps

```python
import spotipy
from spotipy.oauth2 import SpotifyOAuth

from Modules.playlist import Playlist, POPULARITY

if __name__ == "__main__":
    # Define the required scope for Spotify API access
    scope = "user-library-read, playlist-modify-public"

    # Create a Spotify instance with authentication
    sp = spotipy.Spotify(auth_manager=SpotifyOAuth(scope=scope,
                                                   client_id='Complete',  # Replace with actual client ID
                                                   client_secret='Complete',  # Replace with actual client secret
                                                   redirect_uri='http://localhost'))

    # Create a Playlist instance with the provided playlist ID and Spotify instance
    playlist = Playlist(playlist_id='Complete', spotify=sp)  # Replace with actual playlist ID

    # Fetch tracks for the playlist from Spotify
    playlist.get_tracks_from_spotify()

    # Fetch additional track features from Spotify
    playlist.get_track_features()

    # Order tracks within the playlist based on popularity
    playlist.order_tracks(True, POPULARITY)  # Order in descending order based on popularity

    # Print the playlist details
    print(playlist)

    # Reorder the tracks on Spotify based on popularity in descending order
    playlist.order_tracks_on_spotify(reverse=True, sort_by=POPULARITY)
```



