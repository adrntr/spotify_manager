import math

from Modules.track import Track

POPULARITY = "popularity"
DANCEABILITY = "danceability"
CUSTOM = "custom"


class Playlist:
    """
    Represents a playlist with methods to manipulate and order its tracks.
    """

    def __init__(self, playlist_id, spotify):
        """
        Initialize a Playlist object.

        :param playlist_id: The ID of the playlist on Spotify.
        :param spotify: A Spotipy instance for interacting with the Spotify Web API.
        """
        self.playlist_id = playlist_id
        self.tracks: list = []
        self.spotify = spotify

    def __str__(self):
        """
        Get a string representation of the playlist.

        :return: A string containing details of tracks in the playlist.
        """
        tracks_str = ""
        for track in self.tracks:
            tracks_str += str(track) + '\n'
        return tracks_str

    def get_tracks_from_spotify(self, offset=0):
        """
        Fetch tracks for the playlist from Spotify.

        :param offset: Offset for pagination while fetching tracks.
        """
        tracks = self.spotify.playlist_items(playlist_id=self.playlist_id, limit=100, offset=offset).get('items')
        self.tracks.extend([Track(pos=idx,
                                  name=track.get('track').get('name'),
                                  artist=track.get('track').get('artists')[0].get('name'),
                                  popularity=track.get('track').get('popularity'),
                                  track_id=track.get('track').get('id'))
                            for idx, track in enumerate(tracks)])
        if len(tracks) == 100:
            self.get_tracks_from_spotify(offset=offset + 100)

    def get_track_features(self, offset=0):
        """
        Fetch additional track features from Spotify.

        :param offset: Offset for pagination while fetching track features.
        """
        tracks_ids = [track.track_id for track in self.tracks[offset:offset + 100]]
        tracks_features = self.spotify.audio_features(tracks=tracks_ids)
        for idx, track in enumerate(self.tracks[offset:offset + 100]):
            try:
                track.danceability = tracks_features[idx].get('danceability')
                track.energy = tracks_features[idx].get('energy')
                track.tempo = tracks_features[idx].get('tempo')
            except IndexError as e:
                print(e, idx, track, len(tracks_features))
                raise IndexError('list out of range')

        if len(tracks_ids) == 100:
            self.get_track_features(offset=offset + 100)

    def order_tracks(self, reverse: bool, sort_by=POPULARITY):
        """
        Order the tracks within the playlist based on certain criteria.

        :param sort_by: The attribute to sort the tracks by.
        :param reverse: True to sort in descending order.
        """
        if sort_by == CUSTOM:
            self.tracks.sort(
                key=lambda track: math.sin((track.energy - 0.15) * math.pi) * math.sin(
                    (track.danceability - 0.15) * math.pi),
                reverse=True)
        else:
            self.tracks.sort(
                key=lambda track: getattr(track, sort_by),
                reverse=reverse)

    def order_tracks_on_spotify(self, reverse: bool, sort_by=POPULARITY):
        """
        Order the tracks within the playlist and update the order on Spotify.

        :param sort_by: The attribute to sort the tracks by.
        :param reverse: True to sort in descending order.
        """
        self.order_tracks(reverse, sort_by)
        self._remove_tracks_from_spotify()
        self._add_tracks_to_spotify()

    def _remove_tracks_from_spotify(self, offset=0):
        """
        Remove tracks from the playlist on Spotify.

        :param offset: Offset for pagination while removing tracks.
        """
        tracks_ids = [track.track_id for track in self.tracks][offset:offset + 100]
        if not tracks_ids:
            return
        self.spotify.playlist_remove_all_occurrences_of_items(playlist_id=self.playlist_id,
                                                              items=tracks_ids)

        if len(tracks_ids) < 100:
            return

        self._remove_tracks_from_spotify(offset=offset + 100)

    def _add_tracks_to_spotify(self, offset=0):
        """
        Add tracks to the playlist on Spotify.

        :param offset: Offset for pagination while adding tracks.
        """
        tracks_ids = [track.track_id for track in self.tracks][offset:offset + 100]
        if not tracks_ids:
            return
        self.spotify.playlist_add_items(playlist_id=self.playlist_id, items=tracks_ids)
        if len(tracks_ids) < 100:
            return
        self._add_tracks_to_spotify(offset=offset + 100)
