import math

from spotipy import Spotify


class Track:
    def __init__(self, track_id, name, pos=None, artist=None, popularity=None):
        self.pos = pos
        self.name = name
        self.artist = artist
        self.popularity = popularity
        self.track_id = track_id
        self.danceability = -1
        self.energy = -1
        self.tempo = -1

    def __str__(self):
        return f'{self.pos:3} - {self.name:50} - {self.artist:40} - {self.popularity:3} - ' \
               f'{self.energy} - {self.danceability} - {math.sin(self.energy * math.pi) * self.popularity * self.danceability} '

    def get_info(self):
        info = Spotify.sp.audio_features([self.track_id])
        track_info = info[0]
        self.danceability = track_info.get('danceability')
        self.energy = track_info.get('energy')
        self.tempo = track_info.get('tempo')
        more_info = Spotify.sp.audio_analysis()

